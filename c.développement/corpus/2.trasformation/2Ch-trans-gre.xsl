<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs" version="2.0"
    xpath-default-namespace="http://www.tei-c.org/ns/1.0">
    <xsl:output method="xml" encoding="UTF-8" indent="yes"/>


    <xsl:template match="TEI">

        <TEI xmlns="http://www.tei-c.org/ns/1.0">
            <!-- Cette feuille de transformation produira un fichier de sortie encodé au format XML/TEI -->

            <teiHeader>

                <!-- Le fichier de sortie contiendra le modèle de TEI header ci-dessous. Dans ce fichier de transformation, le TEI header est finalisé à l'éxtraction de données depuis le TEI header du fichier source. Les données concernant le projet en cours seront rajoutées en phase de modélisation, dans le fichier de sortie généré par cette feuille de transformation. -->

                <fileDesc>

                    <titleStmt>
                        <title/>
                        <respStmt>
                            <resp/>
                            <name/>
                        </respStmt>
                    </titleStmt>

                    <editionStmt>
                        <edition>
                            <date/>
                        </edition>
                        <respStmt>
                            <resp/>
                            <name/>
                        </respStmt>
                    </editionStmt>

                    <publicationStmt>
                        <p>Available under a Creative Commons Attribution-ShareAlike 4.0 International
                            License</p>
                    </publicationStmt>

                    <sourceDesc>
                        <!-- Lorsqu'un document numérique (B) est dérivé d'un autre document numérique (A) contenant un TEI header,  alors le document A est source du document B. La section fileDesc du document A doit être inclue dans le sourceDesc du document B, dans l'élement biblFull. (https://www.tei-c.org/release/doc/tei-p5-doc/fr/html/HD.html)-->
                        <biblFull>
                            <xsl:copy-of select="//fileDesc"/>
                            <xsl:copy-of select="//profileDesc"/>
                        </biblFull>
                    </sourceDesc>
                </fileDesc>

                <encodingDesc>
                    <!-- Lorsqu'un document numérique (B) est dérivé d'un autre document numérique (A) contenant un TEI header,  alors le document A est source du document B. Pour disposer d'informations complètes sur l'encodage de la source et sur les changements apportés dans l'encodage courant, le document B contiendra une nouvelle section encodingDesc basée sur la section encodingDesc du document A. (https://www.tei-c.org/release/doc/tei-p5-doc/fr/html/HD.html)-->
                    <projectDesc>
                        <p/>
                    </projectDesc>
                    <samplingDecl>
                        <p/>
                    </samplingDecl>
                    <editorialDecl>
                        <segmentation>
                            <p/>
                        </segmentation>
                    </editorialDecl>
                    <xsl:copy-of select="//refsDecl"/>
                </encodingDesc>

                <profileDesc>
                    <!-- Lorsqu'un document numérique (B) est dérivé d'un autre document numérique (A) contenant un TEI header,  alors le document A est source du document B. La section profileDesc du document A doit être inclue dans le profileDesc de l'élément B. L'élément profileDesc de l'élément B peut accueillir des informations spécifiques relatives au projet B. (https://www.tei-c.org/release/doc/tei-p5-doc/fr/html/HD.html) -->
                    <creation>
                        <date/>
                        <rs/>
                    </creation>
                    <langUsage/>
                    <textClass>
                        <keywords scheme="...">
                            <term/>
                        </keywords>
                    </textClass>
                </profileDesc>

            </teiHeader>

            <text>

                <body>

                    <div  xml:id="LXX_2Par_ch22" type="textpart" subtype="chapter" n="22">
                        
                        <!-- Le document source contient le texte intégral du livre 2 des Chroniques, dont nous voulons extraire uniquement les chapitres 22 et 23. Pour ce faire, on sélectionne dans le texte source les éléments <p>, contenus dans l'élément <div>, dont l'attribut @subtype a la valeur 'chapter' et dont l'attribut @n a la valeur '22' et '23' -->
                        <xsl:for-each select="//div[@subtype = 'chapter' and @n = '22']//p">

                            <!-- Pour chaque élément <p> du fichier d'entrée, on créé dans le fichier de sortie : un élément <div> avec un attribut @xml:id, qui récupère la valeur de l'attribut @n de l'élément parent ; un attribut @type, qui récupère la valeur de l'attribut @type de l'élément parent ; un attribut @subtype, qui récupère la valeur de l'attribut @subtype de l'élément parent ; et un attibut @n (numéro de verset) qui récupère la valeur de l'attribut @n de l'élément parent. On ajoute l'attribut @xml:id, absent dans le document source, parce que l'identification individuelle de chaque verset permettra, dans une phase succéssive du travail d'encodage, de créer des références intertextuelles entre les textes bibliques. -->
                            <div xml:id="LXX_2Par_ch22_v{parent::div/@n}" type="{parent::div/@type}"
                                subtype="{parent::div/@subtype}" n="{parent::div/@n}">

                                <!-- On enferme dans un élément <p> le texte contenu dans chaque <div> -->
                                <p>

                                    <!-- On insère dans chaque élément <p> un élément <seg> qui reprend la valeur de l'attribut @n (numéro de verset) de l'élément parent. Cette valeur apparaîtra dans le corps de texte di fichier de sortie en tant que système de numérotation des versets : en effet, le format canonique d'édition du texte biblique veut que chaque verset soit précédé de la réference correspondante. -->
                                    <seg><xsl:value-of select="parent::div/@n"/>.</seg>

                                    <xsl:apply-templates/>

                                </p>

                            </div>
                            
                        </xsl:for-each>
                        
                        <!-- On applique le même procédé pour l'extraction du chapitre 23 -->
                        
                        <xsl:for-each select="//div[@subtype = 'chapter' and @n = '23']//p">
                           
                            <div xml:id="LXX_2Par_ch23_v{parent::div/@n}" type="{parent::div/@type}"
                                subtype="{parent::div/@subtype}" n="{parent::div/@n}">
                            
                                <p>
                                
                                    <seg><xsl:value-of select="parent::div/@n"/>.</seg>
                                    
                                    <xsl:apply-templates/>
                                
                                </p>
                            
                            </div>
                        
                        </xsl:for-each>
                        
                    </div>

                </body>

            </text>

        </TEI>

    </xsl:template>

    <!-- Pour exclure de la sortie les éléments <note/> présents dans le document source, on crée un template vide qui match l'élément <note/> -->
    <xsl:template match="//div//note"/>



</xsl:stylesheet>
