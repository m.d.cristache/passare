<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs" version="2.0">
    <xsl:output method="xml" encoding="UTF-8" indent="yes"/>


    <xsl:template match="Tanach">

        <TEI>
            <teiHeader>

                <!-- Le fichier de sortie contiendra le modèle de TEI header ci-dessous. Dans ce fichier de transformation, le TEI header est finalisé à l'éxtraction de données depuis le TEI header du fichier source. Les données concernant le projet en cours seront rajoutées en phase de modélisation, dans le fichier de sortie généré par cette feuille de transformation. -->

                <fileDesc>

                    <titleStmt>
                        <title/>
                        <respStmt>
                            <resp/>
                            <name/>
                        </respStmt>
                    </titleStmt>

                    <editionStmt>
                        <edition>
                            <date/>
                        </edition>
                        <respStmt>
                            <resp/>
                            <name/>
                        </respStmt>
                    </editionStmt>

                    <publicationStmt>
                        <p/>
                    </publicationStmt>

                    <sourceDesc>
                        <!-- Lorsqu'un document numérique (B) est dérivé d'un autre document numérique (A) contenant un TEI header,  alors le document A est source du document B. La section fileDesc du document A doit être inclue dans le sourceDesc du document B, dans l'élement biblFull. (https://www.tei-c.org/release/doc/tei-p5-doc/fr/html/HD.html)-->
                        <biblFull>
                        </biblFull>
                    </sourceDesc>
                </fileDesc>

                <encodingDesc>
                    <!-- Lorsqu'un document numérique (B) est dérivé d'un autre document numérique (A) contenant un TEI header,  alors le document A est source du document B. Pour disposer d'informations complètes sur l'encodage de la source et sur les changements apportés dans l'encodage courant, le document B contiendra une nouvelle section encodingDesc basée sur la section encodingDesc du document A. (https://www.tei-c.org/release/doc/tei-p5-doc/fr/html/HD.html)-->
                    <projectDesc>
                        <p/>
                    </projectDesc>
                    <samplingDecl>
                        <p/>
                    </samplingDecl>
                    <editorialDecl>
                        <segmentation>
                            <p/>
                        </segmentation>
                    </editorialDecl>
                </encodingDesc>

                <profileDesc>
                    <!-- Lorsqu'un document numérique (B) est dérivé d'un autre document numérique (A) contenant un TEI header,  alors le document A est source du document B. La section profileDesc du document A doit être inclue dans le profileDesc de l'élément B. L'élément profileDesc de l'élément B peut accueillir des informations spécifiques relatives au projet B. (https://www.tei-c.org/release/doc/tei-p5-doc/fr/html/HD.html) -->
                    <creation>
                        <date/>
                        <rs/>
                    </creation>
                    <langUsage/>
                    <textClass>
                        <keywords scheme="...">
                            <term/>
                        </keywords>
                    </textClass>
                </profileDesc>

            </teiHeader>

            <text>
                <body>
                    <div xml:id="TM_2Ch_ch22" type="textpart" subtype="chapter" n="22">
                        <xsl:for-each select="//v">
                            <div xml:id="TM_2Ch_ch22_v{parent::v/@n}" type="textpart"
                                subtype="verse" n="{parent::div/@n}">
                                <xsl:for-each select="/w">
                                    <w>
                                        <xsl:apply-templates/>
                                    </w>
                                </xsl:for-each>
                            </div>
                        </xsl:for-each>
                    </div>
                </body>
            </text>
        </TEI>

    </xsl:template>

</xsl:stylesheet>
