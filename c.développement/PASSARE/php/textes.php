<!DOCTYPE html>
<html>

<head lang="fr">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" type="text/css" href="../css/style.css">

    <!-- Test scroll lock -->
    <script src="../js/text_scroll.js"></script>
	
	<!-- L'aspect visuel du site web est basé sur le framework Bootstrap (https://getbootstrap.com/). Pour utiliser Bootstrap dans la page web, on déclare dans le header de la page sa librairie CSS et ses composants JQuery -->
		<!-- Début de la déclaration Bootstrap -->
			<!-- Librairie CSS pour Bootstrap -->
			<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
			<!-- Composants JQuery pour Bootstrap -->
			<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
			<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
			<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
		<!-- Fin de la déclaration Bootstrap -->

	<!-- La fonctionnalité drag&drop des blocs de texte dans la page "Textes" est basée sur le moteur d'affichage Muuri.(https://github.com/haltu/muuri). Pour utiliser Muuri, on déclare sa librairie Javascript dans le header et on rajoute un script à l'intérieur du body-->
		<!-- Début de la librairie Javascript pour Muuri -->
			<script src="https://unpkg.com/web-animations-js@2.3.1/web-animations.min.js"></script>
			<script src="https://unpkg.com/hammerjs@2.0.8/hammer.min.js"></script>
			<script src="https://unpkg.com/muuri@0.7.1/dist/muuri.min.js"></script>
		<!-- Fin de la librairie Javascript pour Muuri -->
	
	<!-- Script pour la fonctionnalité de défilement simultanée (ne fonctionne pas encore).

	<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
		<script>
			$(function() {
				$("#sortable").sortable();
				$("#sortable").disableSelection();
			});
		</script>	
	-->

    <title>Plateforme PASSARE</title>
	
</head>

<body>

	 <div class="projet-contenu-home">
        <?php
                      $xmlDoc = new DOMDocument();
                      $xmlDoc->load("../xml/contenu-home.xml");
                      print $xmlDoc->saveXML();
                     ?>
    </div>

	<!-- Menu de navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark d-flex flex-sm-row" style="background-image:url(../images/lettre.jpg); height: 100px;">
        
		<!-- Logo (gauche) -->
		<a class="navbar-brand" href="#">
            <img src="" height="100" class="d-inline-block float-left" alt="">
        </a>
		
		<!-- Liens de navigation (droite) -->
        <div class="d-flex flex-fill collapse navbar-collapse justify-content-end">

		<ul class="navbar-nav justify-content-end mr-0">
		
			<li class="nav-fill">
				<a class="flex-sm-fill text-sm-center nav-link menu" href="../index.php">Home</a>
			</li>
			
			<li class="nav-fill active">
				<a class="flex-sm-fill text-sm-center nav-link menu" href="../php/textes.php">Textes</a>
			</li>
			
			<li class="nav-fill">
				<a class="flex-sm-fill text-sm-center nav-link menu" href="../php/le-projet.php">Le Projet</a>
			</li>
			
			<li class="nav-fill">
				<a class="flex-sm-fill text-sm-center nav-link menu" href="../php/qui-sommes-nous.php">Qui sommes-nous</a>
			</li>
		</ul>
        </div>
    </nav>

	<!-- Breadcrumbs -->
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb bg-white small">
            <li class="breadcrumb-item">
                <a href="../index.html">Home</a>
            </li>
            <li class="breadcrumb-item active" aria-current="page">Textes</li>
        </ol>
    </nav>

	<!-- Menu lateral -->
	
    <div class="sidenav">
	
        <div class="container d-flex flex-column">

            <div class="btn-group-vertical" role="group">
                <button type="button" class="btn btn-block btn-outline-warning btn-filter btn-sm my-1 py-2">Discordances entre textes hébreux</button>
                <button type="button" class="btn btn-block btn-outline-danger btn-filter btn-sm my-1 py-2">Discordances entre textes grecs</button>
                <button type="button" class="btn btn-block btn-outline-primary btn-filter btn-sm my-1 py-2">Accord de trois témoins contre le quatrième</button>
                <button type="button" class="btn btn-block btn-outline-secondary btn-filter btn-sm my-1 py-2">Différences stylistiques entre les textes grecs</button>
                <button type="button" class="btn btn-block btn-outline-success btn-filter btn-sm my-1 py-2">Leçons propres apportées par la LXX</button>
            </div>

            <div class="btn-group-vertical mt-5 mb-5" role="group">

				<!-- Fonctionnalité de défilement simultanée : script à vérifier -->
                <div class="btn-group d-inline-block d-flex flex-row" role="group">
                    <input id="btn" type="button" class="btn btn-block btn-outline-dark btn-filter btn-sm my-1 py-2" value="Défilement simultanée" onclick="lock()"/>
                    <input id="btn" type="button" class="btn btn-block btn-outline-dark btn-filter btn-sm my-1 py-2" value="Défilement séparé" onclick="unlock()" />
                </div>

                <button type="button" class="btn btn-block btn-outline-dark btn-filter btn-sm my-1">Aligner tous sur le texte sélectionné</button>
            </div>

        </div>

    </div>
	
	<!-- Conteneur des quatre blocs de texte -->
    <div class="grid">

		<!-- Bloc de texte n.1 (en haut à gauche) -->
        <div class="item">

            <div class="item-content">

                <div class="titre-image">

					<!-- Classe pour l'affichage du menu deroulant  contenant la liste des textes disponibles -->
                    <div class="dropdown">
					
						<!-- Bouton pour l'ouverture du menu deroulant -->
                        <button type="button" class="titre-choix-bouton btn btn-sm btn-dark dropdown-toggle" id="dropdownMenuOffset" data-toggle="dropdown" data-offset="-100,0">Texte</button>
						
							<!-- Contenu du menu deroulant -->
							<div class="dropdown-menu liste-titre" aria-labelledby="dropdownMenuOffset">
								<a class="dropdown-item liste-item" onclick="myFunction1(),myFunctionTitre1(),myFunctionTEI1()">4 Règnes</a>
								<a class="dropdown-item liste-item" onclick="myFunction(),myFunctionTitre()">Texte</a>
								<a class="dropdown-item liste-item" onclick="myFunction(),myFunctionTitre()">Texte</a>
								<a class="dropdown-item liste-item" onclick="myFunction(),myFunctionTitre()">Texte</a>
							</div>

							<!-- En-tête du bloc de texte -->
							<!-- Titre du texte sélectionné -->
							<div id="titre1" style="display: none; width: auto; height: 25px; float:left; background-color: rgba(51, 47, 46, 0.91);">
								<div class="titre-texte-bouton">4 Règnes</div>
							</div>
							<!-- Logo TEI pour accès au fichier source -->
							<a href="../corpus/2R-model-gre.xml"><img  id="TEI1" src="../images/TEI-600.jpg" class="titre-logo" style="display: none; width: auto; float:left;"></a>

                    </div>

                </div>

				<!-- Données textuelles affichées dans le bloc de texte -->
                <div id="myDIV1" style="display: none;" class="item-texte syncscroll" name="">

                    <!-- Script PHP pour la lecture des données textuelles contenues dans un fichier XML séparé. Utilise l'analyseur DOM XML (https://www.w3schools.com/php/php_xml_dom.asp)  -->
                    <?php
					  $xmlDoc = new DOMDocument();
					  $xmlDoc->load("../corpus/2R-model-gre.xml");
					  print $xmlDoc->saveXML();

					  // Test pour la lecture du contenu de l'élément body (ne fonctionne pas) :
						  //$xmlDoc = new DOMDocument();
						  //$xmlDoc->load("./2R-model-gre.xml");
						  //$toto = $xmlDoc->getElementsByTagName('body');
						  //print $toto->

					  ?>

                </div>
				
            </div>
			
        </div>
		
		<!-- Bloc de texte n.2 (en haut à droite) -->
        <div class="item">
		
            <div class="item-content">
			
                <div class="titre-image">

					<!-- Classe pour l'affichage du menu deroulant  contenant la liste des textes disponibles -->
                    <div class="dropdown">
					
						<!-- Bouton pour l'ouverture du menu deroulant -->
                        <button type="button" class="titre-choix-bouton btn btn-sm btn-dark dropdown-toggle" id="dropdownMenuOffset" data-toggle="dropdown" data-offset="-100,0">Texte</button>

						<!-- Contenu du menu deroulant -->
                        <div class="dropdown-menu liste-titre" aria-labelledby="dropdownMenuOffset">
                            <a class="dropdown-item liste-item" onclick="myFunction2(),myFunctionTitre2(),myFunctionTEI2()">2 Paralipomènes</a>
                            <a class="dropdown-item liste-item" onclick="myFunction(),myFunctionTitre()">Texte</a>
                            <a class="dropdown-item liste-item" onclick="myFunction(),myFunctionTitre()">Texte</a>
                            <a class="dropdown-item liste-item" onclick="myFunction(),myFunctionTitre()">Texte</a>
                        </div>

						<!-- En-tête du bloc de texte -->
						<!-- Titre du texte sélectionné -->
						<div id="titre2" style="display: none; width: auto; height: 25px; float:left; background-color: rgba(51, 47, 46, 0.91);">
							<div class="titre-texte-bouton">2 Paralipomènes</div>
						</div>
						<!-- Logo TEI pour accès au fichier source -->
						<a href="../corpus/2R-model-gre.xml"><img  id="TEI2" src="../images/TEI-600.jpg" class="titre-logo" style="display: none; width: auto; float:left;"></a>

                    </div>

                </div>
				
				<!-- Données textuelles affichées dans le bloc de texte -->
                <div id="myDIV2" style="display: none;" class="item-texte syncscroll"  name="">
					
					<!-- Script PHP pour la lecture des données textuelles contenues dans un fichier XML séparé. Utilise l'analyseur DOM XML (https://www.w3schools.com/php/php_xml_dom.asp)  -->
                    <?php
					  $xmlDoc = new DOMDocument();
					  $xmlDoc->load("../corpus/2Ch-model-gre.xml");
					  print $xmlDoc->saveXML();
					 ?>

                </div>
				
            </div>
			
        </div>

		<!-- Bloc de texte n.3 (en bas à gauche) -->
        <div class="item">
		
            <div class="item-content">
			
                <div class="titre-image">

                    <div class="dropdown">
                        <button type="button" class="titre-choix-bouton btn btn-sm btn-dark dropdown-toggle disabled" id="dropdownMenuOffset" data-toggle="dropdown" data-offset="-100,0">Texte</button>

                        <div class="dropdown-menu liste-titre" aria-labelledby="dropdownMenuOffset">
                            <a class="dropdown-item liste-item" onclick="myFunction3(),myFunctionTitre3(),myFunctionTEI3()">2 Rois</a>
                            <a class="dropdown-item liste-item" onclick="myFunction(),myFunctionTitre()">Texte</a>
                            <a class="dropdown-item liste-item" onclick="myFunction(),myFunctionTitre()">Texte</a>
                            <a class="dropdown-item liste-item" onclick="myFunction(),myFunctionTitre()">Texte</a>
                        </div>

                            <div id="titre3" style="display: none; width: auto; height: 25px; float:left; background-color: rgba(51, 47, 46, 0.91);">
                                <div class="titre-texte-bouton">2 Rois</div>
                            </div>
                            <a href="../corpus/2R-model-gre.xml"><img  id="TEI3" src="../images/TEI-600.jpg" class="titre-logo" style="display: none; width: auto; float:left;"></a>

                    </div>

                </div>
				
                <div id="myDIV3" style="display: none;" class="item-texte syncscroll"  name="unlock">
                    
					<!-- Script PHP pour la lecture des données textuelles contenues dans un fichier XML séparé. Utilise l'analyseur DOM XML (https://www.w3schools.com/php/php_xml_dom.asp)  -->
                    <?php
					  $xmlDoc = new DOMDocument();
					  $xmlDoc->load("");
					  print $xmlDoc->saveXML();
					 ?>
					 
                </div>
            </div>
        </div>

		<!-- Bloc de texte n.4 (en bas à droite) -->
        <div class="item">
		
            <div class="item-content">
			
                <div class="titre-image">

                    <div class="dropdown">
					
                        <button type="button" class="titre-choix-bouton btn btn-sm btn-dark dropdown-toggle disabled" id="dropdownMenuOffset" data-toggle="dropdown" data-offset="-100,0">Texte</button>

                        <div class="dropdown-menu liste-titre" aria-labelledby="dropdownMenuOffset">
                            <a class="dropdown-item liste-item" onclick="myFunction4(),myFunctionTitre4(),myFunctionTEI4()">2 Chroniques</a>
                            <a class="dropdown-item liste-item" onclick="myFunction(),myFunctionTitre()">Texte</a>
                            <a class="dropdown-item liste-item" onclick="myFunction(),myFunctionTitre()">Texte</a>
                            <a class="dropdown-item liste-item" onclick="myFunction(),myFunctionTitre()">Texte</a>
                        </div>

                            <div id="titre4" style="display: none; width: auto; height: 25px; float:left; background-color: rgba(51, 47, 46, 0.91);">
                                <div class="titre-texte-bouton">2 Chroniques</div>
                            </div>
                            <a href="../corpus/2R-model-gre.xml"><img  id="TEI4" src="../images/TEI-600.jpg" class="titre-logo" style="display: none; width: auto; float:left;"></a>

                    </div>

                </div>
				
                <div id="myDIV4" style="display: none;" class="item-texte syncscroll"  name="unlock">
                    
					<!-- Script PHP pour la lecture des données textuelles contenues dans un fichier XML séparé. Utilise l'analyseur DOM XML (https://www.w3schools.com/php/php_xml_dom.asp)  -->
                    <?php
					  $xmlDoc = new DOMDocument();
					  $xmlDoc->load();
					  print $xmlDoc->saveXML();
					 ?>
					 
                </div>
				
            </div>
			
        </div>

    </div>

	<!-- Footer -->
    <div class="footer">
        <p>
			<!-- Licence du site web -->
            <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Licence Creative Commons" style="border-width:0;height: 20px;" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a>Ce(tte) œuvre est mise à disposition selon les termes de la <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Licence Creative Commons Attribution - Pas d’Utilisation Commerciale - Partage dans les Mêmes Conditions 4.0 International</a>.</p>
    </div>

	
	<!-- ----------------------------------------------- SCRIPTS ----------------------------------------------- -->
	
	<!-- fonction Javascript Muuri pour la fonctionnalité drag&drop des blocs de texte -->
    <script type="text/javascript">
        var grid = new Muuri('.grid', {
            dragEnabled: true,
            layout: {
                horizontal: false
            }
        });
    </script>

	<!-- variable Javascript Muuri pour l'adaptation automatique des blocs de texte à la taille de l'écran -->
    <script>
        var grid = new Muuri(elem, {
            layoutOnResize: true
        });
    </script>

    <!-- fonction Javascript Bootstrap pour l'ouverture du menu deroulant -->
    <script type="text/javascript">
        $().button('toggle')
    </script>

	<!-- fonctions Javascript pour l'ouverture des textes via les liens des menus deroulant dans les blocs de texte-->
	
		<!-- bloc de texte n.1 -->
			<!-- appel du texte (utilise le script PHP)  -->
			<script>
				function myFunction1() {
					var x = document.getElementById("myDIV1");
					if (x.style.display === "none") {
						x.style.display = "block";
					} else {
						x.style.display = "none";
					}
				}
			</script>

			<!-- appel du titre -->
			<script>
				function myFunctionTitre1() {
					var x = document.getElementById("titre1");
					if (x.style.display === "none") {
						x.style.display = "block";
					} else {
						x.style.display = "none";
					}
				}
			</script>

			<!-- appel du logo TEI -->
			<script>
				function myFunctionTEI1() {
					var x = document.getElementById("TEI1");
					if (x.style.display === "none") {
						x.style.display = "block";
					} else {
						x.style.display = "none";
					}
				}
			</script>

		<!-- bloc de texte n.2 -->
			<!-- appel du texte (utilise le script PHP)  -->
			<script>
				function myFunction2() {
					var x = document.getElementById("myDIV2");
					if (x.style.display === "none") {
						x.style.display = "block";
					} else {
						x.style.display = "none";
					}
				}
			</script>
			
			<!-- appel du titre -->
			<script>
				function myFunctionTitre2() {
					var x = document.getElementById("titre2");
					if (x.style.display === "none") {
						x.style.display = "block";
					} else {
						x.style.display = "none";
					}
				}
			</script>
			
			<!-- appel du logo TEI -->
			<script>
				function myFunctionTEI2() {
					var x = document.getElementById("TEI2");
					if (x.style.display === "none") {
						x.style.display = "block";
					} else {
						x.style.display = "none";
					}
				}
			</script>

		<!-- bloc de texte n.3 -->
			<!-- appel du texte (utilise le script PHP)  -->
			<script>
				function myFunction3() {
					var x = document.getElementById("myDIV3");
					if (x.style.display === "none") {
						x.style.display = "block";
					} else {
						x.style.display = "none";
					}
				}
			</script>
			
		<!-- appel du titre -->
		<script>
			function myFunctionTitre3() {
				var x = document.getElementById("titre3");
				if (x.style.display === "none") {
					x.style.display = "block";
				} else {
					x.style.display = "none";
				}
			}
		</script>
		
		<!-- appel du logo TEI -->
		<script>
			function myFunctionTEI3() {
				var x = document.getElementById("TEI3");
				if (x.style.display === "none") {
					x.style.display = "block";
				} else {
					x.style.display = "none";
				}
			}
		</script>
		
	<!-- bloc de texte n.4 -->
		<!-- appel du texte (utilise le script PHP)  -->
		<script>
			function myFunction4() {
				var x = document.getElementById("myDIV4");
				if (x.style.display === "none") {
					x.style.display = "block";
				} else {
					x.style.display = "none";
				}
			}
		</script>
		
		<!-- appel du titre -->
		<script>
			function myFunctionTitre4() {
				var x = document.getElementById("titre4");
				if (x.style.display === "none") {
					x.style.display = "block";
				} else {
					x.style.display = "none";
				}
			}
		</script>
		
		<!-- appel du logo TEI -->
		<script>
			function myFunctionTEI4() {
				var x = document.getElementById("TEI4");
				if (x.style.display === "none") {
					x.style.display = "block";
				} else {
					x.style.display = "none";
				}
			}
		</script>

		<!-- fonctions Javascript pour la fonctionnalité de défilement automatique (ne fonctionne pas encore) -->
    <script type="text/javascript">
        function unlock() {
            document.getElementById("myDIV1").removeAttribute("name");
            document.getElementById("myDIV2").removeAttribute("name");
            document.getElementById("myDIV3").removeAttribute("name");
            document.getElementById("myDIV4").removeAttribute("name");
            syncscroll.reset();
        }
        function lock() {
            document.getElementById("myDIV1").setAttribute("name", "lock");
            document.getElementById("myDIV2").setAttribute("name", "lock");
            document.getElementById("myDIV3").setAttribute("name", "lock");
            document.getElementById("myDIV4").setAttribute("name", "lock");
            syncscroll.reset();
        }
    </script>
</body>

</html>