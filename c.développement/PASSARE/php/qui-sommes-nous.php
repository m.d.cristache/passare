<!DOCTYPE html>
<html>

<head lang="fr">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" type="text/css" href="../css/style.css">
    
	<!-- L'aspect visuel du site web est basé sur le framework Bootstrap (https://getbootstrap.com/). Pour utiliser Bootstrap dans la page web, on déclare dans le header de la page sa librairie CSS et ses composants JQuery -->
		<!-- Début de la déclaration Bootstrap -->
			<!-- Librairie CSS pour Bootstrap -->
			<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
			<!-- Composants JQuery pour Bootstrap -->
			<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
			<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
			<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
		<!-- Fin de la déclaration Bootstrap -->

    <title>Plateforme PASSARE</title>
</head>

<body>

     <div class="projet-contenu-home">
        <?php
                      $xmlDoc = new DOMDocument();
                      $xmlDoc->load("../xml/contenu-home.xml");
                      print $xmlDoc->saveXML();
                     ?>
    </div>

	<!-- Menu de navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark d-flex flex-sm-row" style="background-image:url(../images/lettre.jpg); height: 100px;">
	
		<!-- Logo (gauche) -->
        <a class="navbar-brand" href="#">
            <img src="" height="100" class="d-inline-block float-left" alt="">
        </a>
		
		<!-- Liens de navigation (droite) -->
        <div class="d-flex flex-fill collapse navbar-collapse justify-content-end">

            <ul class="navbar-nav justify-content-end mr-0">
			
                <li class="nav-fill">
                    <a class="flex-sm-fill text-sm-center nav-link menu" href="../index.php">Home</a>
                </li>
				
                <li class="nav-fill">
                    <a class="flex-sm-fill text-sm-center nav-link menu" href="../php/textes.php">Textes</a>
                </li>
				
                <li class="nav-fill">
                    <a class="flex-sm-fill text-sm-center nav-link menu" href="../php/le-projet.php">Le Projet</a>
                </li>
				
                <li class="nav-fill active">
                    <a class="flex-sm-fill text-sm-center nav-link menu" href="../php/qui-sommes-nous.php">Qui sommes-nous</a>
                </li>
				
            </ul>
			
        </div>
		
    </nav>

	<!-- Breadcrumbs -->
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb bg-white small">
            <li class="breadcrumb-item">
                <a href="../index.html">Home</a>
            </li>
            <li class="breadcrumb-item active" aria-current="page">Qui sommes-nous</li>
        </ol>
    </nav>

	<!-- Footer -->
    <div class="footer">
        <p>
			<!-- Licence du site web -->
            <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Licence Creative Commons" style="border-width:0;height: 20px;" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a>Ce(tte) œuvre est mise à disposition selon les termes de la <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Licence Creative Commons Attribution - Pas d’Utilisation Commerciale - Partage dans les Mêmes Conditions 4.0 International</a>.</p>
    </div>

</body>

</html>